/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE TABLE `binh_luan` (
  `binh_luan_id` int NOT NULL AUTO_INCREMENT,
  `nguoi_dung_id` int DEFAULT NULL,
  `hinh_id` int DEFAULT NULL,
  `ngay_binh_luan` datetime DEFAULT NULL,
  `noi_dung` varchar(400) DEFAULT NULL,
  PRIMARY KEY (`binh_luan_id`),
  KEY `nguoi_dung_id` (`nguoi_dung_id`),
  KEY `hinh_id` (`hinh_id`),
  CONSTRAINT `binh_luan_ibfk_1` FOREIGN KEY (`nguoi_dung_id`) REFERENCES `nguoi_dung` (`nguoi_dung_id`),
  CONSTRAINT `binh_luan_ibfk_2` FOREIGN KEY (`hinh_id`) REFERENCES `hinh_anh` (`hinh_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `hinh_anh` (
  `hinh_id` int NOT NULL AUTO_INCREMENT,
  `ten_hinh` varchar(400) DEFAULT NULL,
  `duong_dan` varchar(400) DEFAULT NULL,
  `mo_ta` varchar(400) DEFAULT NULL,
  `nguoi_dung_id` int DEFAULT NULL,
  PRIMARY KEY (`hinh_id`),
  KEY `nguoi_dung_id` (`nguoi_dung_id`),
  CONSTRAINT `hinh_anh_ibfk_1` FOREIGN KEY (`nguoi_dung_id`) REFERENCES `nguoi_dung` (`nguoi_dung_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `luu_anh` (
  `nguoi_dung_id` int NOT NULL,
  `hinh_id` int NOT NULL,
  `ngay_luu` datetime DEFAULT NULL,
  PRIMARY KEY (`nguoi_dung_id`,`hinh_id`),
  KEY `hinh_id` (`hinh_id`),
  CONSTRAINT `luu_anh_ibfk_1` FOREIGN KEY (`nguoi_dung_id`) REFERENCES `nguoi_dung` (`nguoi_dung_id`),
  CONSTRAINT `luu_anh_ibfk_2` FOREIGN KEY (`hinh_id`) REFERENCES `hinh_anh` (`hinh_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `nguoi_dung` (
  `nguoi_dung_id` int NOT NULL AUTO_INCREMENT,
  `email` varchar(400) DEFAULT NULL,
  `mat_khau` varchar(400) DEFAULT NULL,
  `ho_ten` varchar(400) DEFAULT NULL,
  `tuoi` int DEFAULT NULL,
  `anh_dai_dien` varchar(400) DEFAULT NULL,
  PRIMARY KEY (`nguoi_dung_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `binh_luan` (`binh_luan_id`, `nguoi_dung_id`, `hinh_id`, `ngay_binh_luan`, `noi_dung`) VALUES
(1, 1, 3, '2020-02-13 23:40:20', 'meeeeeeeee');
INSERT INTO `binh_luan` (`binh_luan_id`, `nguoi_dung_id`, `hinh_id`, `ngay_binh_luan`, `noi_dung`) VALUES
(2, 1, 2, '2020-02-13 23:45:21', 'helooooooo');
INSERT INTO `binh_luan` (`binh_luan_id`, `nguoi_dung_id`, `hinh_id`, `ngay_binh_luan`, `noi_dung`) VALUES
(3, 1, 4, '2020-02-15 23:47:20', 'huuuuuuuuuuuuu');
INSERT INTO `binh_luan` (`binh_luan_id`, `nguoi_dung_id`, `hinh_id`, `ngay_binh_luan`, `noi_dung`) VALUES
(4, 1, 1, '2020-02-22 23:03:30', 'ummmmmmmmm'),
(5, 2, 2, '2020-02-22 21:03:30', 'Em ăn cơm chưa ?'),
(6, 7, 1, '2023-11-06 19:12:21', 'le sang'),
(7, 7, 1, '2023-11-06 19:12:47', 'le sang'),
(8, 7, 1, '2023-11-06 19:13:16', 'hè lố'),
(9, 7, 1, '2023-11-06 19:17:43', 'hè lố'),
(10, 7, 1, '2023-11-06 19:19:54', ' bi bi'),
(11, 7, 1, '2023-11-15 03:01:41', ' bi bi'),
(12, 9, 3, '2023-11-15 03:02:10', ' bi bi'),
(13, 11, 3, '2023-11-15 03:45:51', ' bi bi'),
(14, 11, 3, '2023-11-15 04:20:37', ' hello');

INSERT INTO `hinh_anh` (`hinh_id`, `ten_hinh`, `duong_dan`, `mo_ta`, `nguoi_dung_id`) VALUES
(1, 'abc', 'https://cdn3.dhht.vn/wp-content/uploads/2023/09/35-canh-dep-sapa-nhat-dinh-khong-duoc-bo-lo-12.jpg', 'abcbfjkkjgdhjhsbjh', 1);
INSERT INTO `hinh_anh` (`hinh_id`, `ten_hinh`, `duong_dan`, `mo_ta`, `nguoi_dung_id`) VALUES
(2, 'dalat', 'https://cdn.vntrip.vn/cam-nang/wp-content/uploads/2017/10/thung-lung-muong-hoa-canh-dep-sapa.jpg', 'Đà Lạt là thành phố tỉnh lỵ trực thuộc tỉnh Lâm Đồng, nằm trên cao nguyên Lâm Viên', 1);
INSERT INTO `hinh_anh` (`hinh_id`, `ten_hinh`, `duong_dan`, `mo_ta`, `nguoi_dung_id`) VALUES
(3, 'vungtau', 'https://owa.bestprice.vn/images/articles/uploads/top-15-dia-diem-nen-di-o-sapa-duoc-yeu-thich-nhat-5f1e52b0b2823.jpg', 'Vũng Tàu là một thành phố thuộc tỉnh Bà Rịa – Vũng Tàu', 2);
INSERT INTO `hinh_anh` (`hinh_id`, `ten_hinh`, `duong_dan`, `mo_ta`, `nguoi_dung_id`) VALUES
(4, 'nha_trang', 'https://cdnmedia.baotintuc.vn/Upload/XjfgEPYM30O8z6jY3MHxSw/files/2021/09/2209/Fansipan-8.jpg', 'Nha Trang là một thành phố ven biển ', 1),
(6, 'Quy_nhon', 'https://statzpack.com/wp-content/uploads/2021/10/20.10Nhung-ly-do-ma-ban-khong-nen-bo-lo-canh-dep-sapa-mua-xuan.jpg', 'Quy Nhơn là một thành phố lớn ven biển vùng duyên hải Nam Trung Bộ', 5),
(7, 'Ky_co', 'https://www.vietnambooking.com/wp-content/uploads/2018/01/le-hoi-hoa-do-quyen-sapa.jpg', ' Kỳ Co - Eo Gió là combo địa điểm mà bất cứ ai du lịch Quy Nhơn', 7),
(8, '1699975806669', '1699975806651_Capture.PNG', NULL, 7),
(9, '1699975990883', '1699975990869_conan_chibi.jpg', '\"hihi\"', 7),
(10, '1699976008325', '1699976008316_conan_chibi.jpg', '\"hihi\"', 7),
(11, '1699976034734', '1699976034725_conan_chibi.jpg', 'hihi', 7),
(12, '1699980569045', '1699980569032_apple-button.png', 'hihi', 7),
(16, '1700018098465', '1700018098439_z4868028610309_d0d7fbb45de3bbdd83e2e0f4a4b53686.jpg', 'abcd', 7),
(17, '1700018196117', '1700018196096_z4868028610309_d0d7fbb45de3bbdd83e2e0f4a4b53686.jpg', 'abcd', 9),
(18, '1700022292300', '1700022292283_Content.png', 'abcd', 11);

INSERT INTO `luu_anh` (`nguoi_dung_id`, `hinh_id`, `ngay_luu`) VALUES
(2, 3, '2020-05-21 21:33:22');
INSERT INTO `luu_anh` (`nguoi_dung_id`, `hinh_id`, `ngay_luu`) VALUES
(7, 1, '2020-05-21 21:33:22');
INSERT INTO `luu_anh` (`nguoi_dung_id`, `hinh_id`, `ngay_luu`) VALUES
(11, 9, '2020-05-21 21:33:22');

INSERT INTO `nguoi_dung` (`nguoi_dung_id`, `email`, `mat_khau`, `ho_ten`, `tuoi`, `anh_dai_dien`) VALUES
(1, 'thinh@gmail.com', '$2b$10$1ewKZTf7ugGY1rQmYlWsbe9Fbs2KVYKTT4z726y0GzAYsN5VnABxu', 'Nguyuyễn Văn Thịnh', 20, '');
INSERT INTO `nguoi_dung` (`nguoi_dung_id`, `email`, `mat_khau`, `ho_ten`, `tuoi`, `anh_dai_dien`) VALUES
(2, 'annhien123@gmail.com', '$2b$10$OcfniK..ahCR6K5DK347e.HNNruqxkvxkpKNsBgbtY9mCEiendB4y', 'Trần An Nhiên', 23, '');
INSERT INTO `nguoi_dung` (`nguoi_dung_id`, `email`, `mat_khau`, `ho_ten`, `tuoi`, `anh_dai_dien`) VALUES
(3, 'maithnah@gmail.com', '$2b$10$AdjAVV7Z50vz4QLvUTMS4.xfLg9svVZaNHLCmrwthbaZG9uhhEmFO', 'Mai Thị Thanh', 25, '');
INSERT INTO `nguoi_dung` (`nguoi_dung_id`, `email`, `mat_khau`, `ho_ten`, `tuoi`, `anh_dai_dien`) VALUES
(4, 'mixu@gmail.com', '$2b$10$w2UNhS9EWhYGDMr05fSEvee8N9LDSIu1FT/OjYV/D5fI5BUxV3uWS', 'Mi Xu', 19, ''),
(5, 'nam@gmail.com', '$2b$10$sEDeMB2pHYVw6To.VO/9bOVNR0crRe4UsD4fm1SSpO2QxpLqfRvy2', 'Nguyễn Văn Nam', 26, ''),
(6, 'hoa@gmail.com', '$2b$10$ZUOyvbfkvlLJLJMPu1lDA.kvMOveU.8F/sZ2lllIBt75l2ElHIDuG', 'Nguyễn Thị Hoa', 26, ''),
(7, 'mimim@gmail.com', '$2b$10$xvwGe1CgkvLuSvZnw3jFSOuR036GEDGleFhU29Ax4in5XMvr.RY2q', 'Mi Mi', 22, '1700017998653_Untitled.png'),
(8, 'gigi@gmail.com', '$2b$10$5U15ek017VtCSIeNW4iX0.4XrY0BlCxoMuU8Fn6NL1ObLMmyX44MG', 'Gi', 25, ''),
(9, 'lilim@gmail.com', '$2b$10$ppzk2Mxgsw34rIImvVhjle5Zzap4Oek6DdpfqmBUyvfyqSOPPSbdu', 'Li Li', 22, ''),
(10, 'hehe@gmail.com', '$2b$10$/ZmTihSxY29pHYO20P1beO0VSqruz9jP98QIbLgJDG88Zd6AIQni.', 'he', 21, ''),
(11, 'memem@gmail.com', '$2b$10$ppzk2Mxgsw34rIImvVhjle5Zzap4Oek6DdpfqmBUyvfyqSOPPSbdu', 'meme1234', 23, '1700022255907_Avatar.png');


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;