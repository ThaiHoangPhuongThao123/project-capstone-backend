import express from "express";

const app = express();
app.use(express.json());
app.use(express.static("."));

import cors from "cors";
app.use(cors());
import rootRouter from "./src/routers/rootRouter.js";

app.listen(8080);
app.use(rootRouter)