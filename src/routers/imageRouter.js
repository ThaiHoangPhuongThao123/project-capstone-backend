import express from "express";
import { createImage, deleteImage, getCommentByIdImage, getDetailInfoByIdImage, getImage, getSavedByIdImage, searchNameImage } from "../controllers/imageCotroller.js";
import { upload } from "../controllers/uploadController.js";
import { checkApi, checkToken } from "../config/jwt.js";
const imageRouter = express.Router();

imageRouter.get("/get-image", checkApi, getImage);
imageRouter.get("/search-name-image/:keyword", checkApi,searchNameImage);

// lấy thông tin ảnh và người tạo ảnh  bằng id ảnh
imageRouter.get("/get-info-by-idImage/:idImage",checkApi, getDetailInfoByIdImage)

// lấy thông tin bình luận theo id ảnh
imageRouter.get("/get-comment-by-idImage/:idImage",checkApi, getCommentByIdImage)


// lấy thông tin đã luu hình này chưa theo id ảnh
imageRouter.get("/get-saved-by-idImage/:idImage",checkApi, getSavedByIdImage)

// xóa ảnh đã tọa theo id ảnh
imageRouter.delete("/delete-image/:idImage", checkApi,deleteImage);

imageRouter.post("/create-image",checkApi, upload.single("file"), createImage)
export default imageRouter;