import express from "express";
import { updateAvatar, getInfoUser, getListCreatedImageByUserId, getListSavedImageByUserId, saveComment, updateUserInfo, userLogin, userRegister } from "../controllers/userController.js";
import { upload } from "../controllers/uploadController.js";
import { checkApi } from "../config/jwt.js";

const userRouter = express.Router();
userRouter.post("/user-login", userLogin);

userRouter.post("/user-register", userRegister);

// lưu thông tin bình luận của người dùng với hình ảnh
userRouter.post("/saved-comment/:idImage", checkApi,saveComment);

// lấy thông tin user
userRouter.get("/get-user-info", checkApi,getInfoUser);

// lấy danh sách ảnh đã lưu theo user id 
userRouter.get("/get-list-saved-image-by-userId", checkApi,getListSavedImageByUserId);

// lấy danh sách ảnh đã tạo theo user id
userRouter.get("/get-list-created-image-by-userId", checkApi,getListCreatedImageByUserId);

// chỉnh sửa thông tin cá nhân 
userRouter.put("/update-user-info", checkApi,updateUserInfo)

// chỉnh sửa thông tin cá nhân (avatar)
userRouter.put("/update-avatar", checkApi,upload.single("file"), updateAvatar)

export default userRouter;