import { createToken, decodeToken } from "../config/jwt.js";
import sequelize from "../models/connect.js";
import initModels from "../models/init-models.js";
import bcrypt from "bcrypt";
import moment from "moment/moment.js";

const model = initModels(sequelize);
const userLogin = async (req, res) => {
  const { email, mat_khau } = req.body;
  // check email
  const checkEmail = await model.nguoi_dung.findOne({
    where: {
      email: email,
    },
  });

  if (checkEmail) {
    let checkPass = await bcrypt.compare(mat_khau, checkEmail.mat_khau);
    // check mat khau
    if (checkPass) {
      let token = createToken(checkEmail);
      res.send(token);
    } else {
      res.send("Mật khẩu không đúng");
    }
    return;
  }
  res.send("Email không đúng");
  return;
};

const userRegister = async (req, res) => {
  const { email, mat_khau, ho_ten, tuoi } = req.body;
  const checkEmail = await model.nguoi_dung.findOne({
    where: {
      email: email,
    },
  });
  if (checkEmail) {
    res.send("Email đã tồn tại");
    return;
  }
  let newPassword = bcrypt.hashSync(mat_khau, 10);
  let newData = {
    email,
    mat_khau: newPassword,
    ho_ten,
    tuoi,
    anh_dai_dien: "",
  };

  await model.nguoi_dung.create(newData);
  res.send("Đăng ký thành công");
};
const saveComment = async (req, res) => {
  let { idImage } = req.params;
  let { token } = req.headers;
  let { data } = decodeToken(token);
  let { nguoi_dung_id } = data;
  let date = moment().format("YYYY-MM-DD hh:mm:ss");
  let { noi_dung } = req.body;
  let comment = {
    nguoi_dung_id: nguoi_dung_id,
    hinh_id: idImage,
    ngay_binh_luan: date,
    noi_dung,
  };

  await model.binh_luan.create(comment);
  res.send(comment);
};

const getInfoUser = async (req, res) => {
  let { token } = req.headers;
  let { data } = decodeToken(token);
  let { nguoi_dung_id } = data;
  let user = await model.nguoi_dung.findOne({
    where: {
      nguoi_dung_id,
    },
  });
  res.send(user);
};

const getListSavedImageByUserId = async (req, res) => {
  let { token } = req.headers;
  let { data } = decodeToken(token);
  let { nguoi_dung_id } = data;
  let imageSave = await model.luu_anh.findAll({
    where: {
      nguoi_dung_id,
    },
    include: ["hinh"],
  });
  if (imageSave.length > 0) {
    res.send(imageSave);
    return
  }
    res.send("Bạn chưa lưu hình nào");
  
};

const getListCreatedImageByUserId = async (req, res) => {
  const { token } = req.headers;
  let { data } = decodeToken(token);
  let { nguoi_dung_id } = data;
  let images = await model.hinh_anh.findAll({
    where: {
      nguoi_dung_id,
    },
  });
  if (images.length > 0) {
    res.send(images);
    return
  }
  res.send("Bạn chưa tạo hình nào");
};

const updateUserInfo = async (req, res) => {
  let { email, mat_khau, ho_ten, tuoi } = req.body;
  const { token } = req.headers;
  let { data } = decodeToken(token);
  let { nguoi_dung_id } = data;
  let getUser = await model.nguoi_dung.findOne({
    where: {
      nguoi_dung_id,
    },
  });
  let updateUser = { ...getUser, email, mat_khau, ho_ten, tuoi };
  await model.nguoi_dung.update(updateUser, {
    where: {
      nguoi_dung_id,
    },
  });
  res.send("cập nhập thành công");
};
const updateAvatar = async (req, res) => {  
  let file = req.file;
  const { token } = req.headers;
  let { data } = decodeToken(token);
  let { nguoi_dung_id } = data;
  let getUser = await model.nguoi_dung.findOne({
    where: {
      nguoi_dung_id,
    },
  });
  let updateUser = { ...getUser, anh_dai_dien: file?.filename || ""};
   
  await model.nguoi_dung.update(updateUser, {
    where: {
        nguoi_dung_id,
      }
    })
  res.send(file);
 }

export {
  userLogin,
  userRegister,
  saveComment,
  getInfoUser,
  getListSavedImageByUserId,
  getListCreatedImageByUserId,
  updateUserInfo,
  updateAvatar
};
