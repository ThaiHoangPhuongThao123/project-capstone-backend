import { decodeToken } from "../config/jwt.js";
import sequelize from "../models/connect.js";
import initModels from "../models/init-models.js";
import { Sequelize } from "sequelize";

const model = initModels(sequelize);
let Op = Sequelize.Op;
const getImage = async (req, res) => {
  let data = await model.hinh_anh.findAll();
  res.send(data);
};

const searchNameImage = async (req, res) => {
  let { keyword } = req.params;
  let data = await model.hinh_anh.findAll({
    where: {
      ten_hinh: {
        [Op.like]: `%${keyword}%`,
      },
    },
  });
  res.send(data);
};
const getDetailInfoByIdImage = async (req, res) => {
  let { idImage } = req.params;
  let data = await model.hinh_anh.findOne({
    where: {
      hinh_id: idImage,
    },
    include: ["nguoi_dung"],
  });
  res.send(data);
};

const getCommentByIdImage = async (req, res) => {
  const { idImage } = req.params;
  let data = await model.hinh_anh.findOne({
    where: {
      hinh_id: idImage,
    },
    include: ["binh_luans"],
  });
  res.send(data);
};

const getSavedByIdImage = async (req, res) => {
  const { idImage } = req.params;
  const { token } = req.headers;
  const { data } = decodeToken(token);
  let { nguoi_dung_id } = data;
  let image = await model.luu_anh.findOne({
    where: {
      hinh_id: idImage,
      nguoi_dung_id,
    },
    include: ["hinh"],
  });

  if (image) {
    res.send(image);
    return;
  }
  res.send("Bạn chưa lưu hình này");
};

const deleteImage = async (req, res) => {
  let { idImage } = req.params;
  let { token } = req.headers;
  let { data } = decodeToken(token);
  let { nguoi_dung_id } = data;
  let image = await model.hinh_anh.findOne({
    where: {
      nguoi_dung_id,
      hinh_id: idImage,
    },
  });

  if (image) {
    await model.hinh_anh.destroy({
      where: {
        nguoi_dung_id,
        hinh_id: idImage,
      },
    });
    res.send("Xóa thành công hinh ảnh đã tạo");
    return;
  }

  res.send("xóa thất bại");
};

//thêm 1 ảnh của user

const createImage = async (req, res) => { 
  let file = req.file;
  let {  mo_ta } = req.body;
  let { token } = req.headers;
  let { data } = decodeToken(token);
  let { nguoi_dung_id } = data;
  let newImage = {
    ten_hinh: new Date().getTime() + nguoi_dung_id,
    duong_dan: file?.filename, 
    mo_ta: mo_ta || "", 
    nguoi_dung_id,
  }
  await model.hinh_anh.create(newImage);
  res.send("Đã thêm 1 ảnh")
 }


export {
  getImage,
  searchNameImage,
  getDetailInfoByIdImage,
  getCommentByIdImage,
  getSavedByIdImage,
  deleteImage,
  createImage
};
