import jwt from "jsonwebtoken";

// tạo token
export const createToken = (data) => {
  let token = jwt.sign({ data }, "TOKEN", {
    algorithm: "HS256",
    expiresIn: "10000d",
  });
    return token;
};

// CHECK TOKEN
export const checkToken = (token) => { 
    return jwt.verify(token, "TOKEN")
 }

// decode token 
export const decodeToken = (token) => {
    return jwt.decode(token);
};

export const checkApi = (req, res, next) => { 
    let { token } = req.headers;
   try {
   checkToken(token)
        next()
   } catch (exception) {
       res.status(401).send(exception.message)
   }

 }

